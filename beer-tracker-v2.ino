// -----------------
// Get number of beers consumed
// -----------------

//includes

// include REST API library
#include "rest_client.h"
//#include "rest_client.cpp"

//include elapsed time library
#include "elapsedMillis.h"

// Setup variables

//RESTful endpoint domain
RestClient client = RestClient("beerapp.werkdev.co");

//Use a local IP and an explicit port:
//RestClient client = RestClient("192.168.1.50",5000);

//server response
String responseOne;
String responseTwo;

// number of flow meter pulses per liter
//int pulsesPerLiter = 450; //450 according to https://www.adafruit.com/products/828

// setup pulse count from flow meter
volatile int pulsesOne = 0;
volatile int pulsesTwo = 0;

//declare global if you don't want it reset every time loop runs
elapsedMillis timeElapsed; 

// delay in milliseconds between POSTs
unsigned int interval = 15000; //should be 15 seconds

void setup()
{
    //initialize serial communication
    Serial.begin(9600);
    
    // Register a Spark variable here
    //Spark.variable("beers", &beers, INT);
    
    // Connect flow meter to D1 to be an input
    pinMode(D1, INPUT_PULLUP);
    
    // Connect flow meter to D2 to be an input
    pinMode(D2, INPUT_PULLUP);    
    
    //attach interupt event for flow meter pulse 
    
    //fires pulse when D1 goes from low to high
    attachInterrupt(D1, pulseOne, RISING);
    
    //fires pulse when D2 goes from low to high
    attachInterrupt(D2, pulseTwo, RISING);    
}

//fired when D0 goes from low to high 
void pulseOne() 
{
    //add a pulse to pulses
    pulsesOne = pulsesOne + 1;
}

//fired when D1 goes from low to high 
void pulseTwo() 
{
    //add a pulse to pulses
    pulsesTwo = pulsesTwo + 1;
}

//main loop - runs continously after setup
//this loop checks every 15 seconds to see if any beer has been poured
//if beer has been poured than an attempt is made to POST the amount and sensor info to the server
//on success pulses are reset 
//if no success response is received 
void loop()
{
    if (timeElapsed > interval) 
    {		
        //@TODO not sure how to DRY this out
        //questions on what happens if both taps are running at the same time
        
        //attempt to send
        if ( pulsesOne > 0 ) 
        {
            //clear response
            responseOne = "";
            
            //capture pulses to be sent
            int sentPulsesOne = pulsesOne;            
            
            char paramsOne[64];
            sprintf(paramsOne, "pulses=%i&sensor=%i&key=%s", sentPulsesOne, 1, "RyG2CTMsh7qy" );
            
            //do our REST POST here
            int statusCodeOne = client.post("/api/v1/beers", paramsOne, &responseOne);
            
            //print response
            Serial.println("StatusCodeOne: ");
            Serial.println(statusCodeOne);
            
            //print response
            Serial.println("ResponseOne: ");
            Serial.println(responseOne);
            
            //reset pulses if response was good
            if ( statusCodeOne == 200 ) 
            {
                pulsesOne = pulsesOne - sentPulsesOne;
                
                //if for some crazy reason pulsesOne is less than 0, reset it
                if ( pulsesOne < 0 )
                {
                    pulsesOne = 0;
                }                
            }
        }
        else
        {
            Serial.println("no pulses from sensor 1");
        }        
        
        if ( pulsesTwo > 0 ) 
        {
            //clear response
            responseTwo = "";
            
            //capture pulses to be sent
            int sentPulsesTwo = pulsesTwo;                
            
            char paramsTwo[64];
            sprintf(paramsTwo, "pulses=%i&sensor=%i&key=%s", sentPulsesTwo, 2, "RyG2CTMsh7qy" );
            
            //do our REST POST here
            int statusCodeTwo = client.post("/api/v1/beers", paramsTwo, &responseTwo);
            
            //print response
            Serial.println("StatusCodeTwo: ");
            Serial.println(statusCodeTwo);
            
            //print response
            Serial.println("responseCodeOne: ");
            Serial.println(responseTwo);
            
            //reset pulses if response was good
            if ( statusCodeTwo == 200 ) 
            {
                 pulsesTwo = pulsesTwo - sentPulsesTwo;
                
                //if for some crazy reason pulsesOne is less than 0, reset it
                if ( pulsesTwo < 0 )
                {
                    pulsesTwo = 0;
                }
            }
        }
        else
        {
            Serial.println("no pulses from sensor 2");
        }
            
        // reset the counter to 0 so the counting starts over...
        timeElapsed = 0;             
    }
}
